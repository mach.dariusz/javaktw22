package com.sda.javaktw22.wprowadzenie.day1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class MathUtilsTest {

    @DisplayName("Passed number less than 0, should throws exception")
    @Test
    void test_1() throws WrongNumberException {
        Assertions.assertThrows(
                WrongNumberException.class,
                () -> MathUtils.calcFactorial(-1)
        );
    }

    @DisplayName("Passed 0, should return 1")
    @Test
    void test_2() throws WrongNumberException {
        Assertions.assertEquals(
                1,
                MathUtils.calcFactorial(0)
        );
    }

    @DisplayName("Passed 3, should return 6")
    @Test
    void test_3() throws WrongNumberException {
        Assertions.assertEquals(
                6,
                MathUtils.calcFactorial(3)
        );
    }

    @DisplayName("calcFactorial() with for loop")
    @Test
    void test_4() {
        Assertions.assertThrows(
                WrongNumberRuntimeException.class,
                () -> MathUtils.calcFactorialWithForLoop(-1)
        );
        Assertions.assertEquals(
                1,
                MathUtils.calcFactorialWithForLoop(0)
        );
        Assertions.assertEquals(
                6,
                MathUtils.calcFactorialWithForLoop(3)
        );
    }
}