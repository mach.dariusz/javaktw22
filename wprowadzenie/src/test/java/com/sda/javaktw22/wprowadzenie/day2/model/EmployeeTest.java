package com.sda.javaktw22.wprowadzenie.day2.model;

import com.sda.javaktw22.wprowadzenie.day2.service.PayrollService;
import com.sda.javaktw22.wprowadzenie.day2.service.PayrollServiceImpl;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    @DisplayName("create employee and check if id is unique")
    @Test
    void test_1() {
        Employee e1 = new Employee();
        Employee e2 = new Employee("Mariusz", "Dach");
        Employee e3 = new Employee("Dariusz", "Mach", Department.IT);

        assertEquals(1, e1.getId());
        assertEquals(2, e2.getId());
        assertEquals(3, e3.getId());

        System.out.println(e1);
        System.out.println(e2);
        System.out.println(e3);
    }

    @DisplayName("calc salary for employee")
    @Test
    void test_2() {
        Employee emp = new Employee("Dariusz", "Mach", Department.IT);
        emp.setSalary(10000.00);

        PayrollService payrollService = new PayrollServiceImpl();
        assertEquals(8200.00, payrollService.calcSalary(emp));
    }

}