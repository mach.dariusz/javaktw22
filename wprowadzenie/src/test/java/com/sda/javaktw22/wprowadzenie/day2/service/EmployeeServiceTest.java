package com.sda.javaktw22.wprowadzenie.day2.service;

import com.sda.javaktw22.wprowadzenie.day2.model.Employee;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeServiceTest {

    @BeforeEach
    void beforeEach() {
        EmployeeService.cleanEmployeesMap();
    }

    @Test
    void getEmployee() {
        EmployeeService.addEmployee(new Employee("Dariusz", "Mach"));
        assertEquals("Dariusz", EmployeeService.getEmployee(1).getFirstName());
    }

    @Test
    void addEmployee() {
        EmployeeService.addEmployee(new Employee());
        EmployeeService.addEmployee(new Employee());
        EmployeeService.addEmployee(new Employee());

        assertEquals(3, EmployeeService.getAllEmployees().size());
    }

    @Test
    void removeEmployee() {
        EmployeeService.addEmployee(new Employee());
        EmployeeService.addEmployee(new Employee());
        EmployeeService.addEmployee(new Employee());

        assertEquals(3, EmployeeService.getAllEmployees().size());

        EmployeeService.removeEmployee(1);
        assertEquals(2, EmployeeService.getAllEmployees().size());
    }
}