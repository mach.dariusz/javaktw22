package com.sda.javaktw22.wprowadzenie.day1;

public class Address {

    private String street;
    private String buildingNumber;
    private String flatNumber;
    private String city;
    private String country;
    private String postalCode;

    public Address() {}

    public Address(String street, String buildingNumber, String flatNumber, String city, String country, String postalCode) {
        this.street = street;
        this.buildingNumber = buildingNumber;
        this.flatNumber = flatNumber;
        this.city = city;
        this.country = country;
        this.postalCode = postalCode;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getBuildingNumber() {
        return buildingNumber;
    }

    public void setBuildingNumber(String buildingNumber) {
        this.buildingNumber = buildingNumber;
    }

    public String getFlatNumber() {
        return flatNumber;
    }

    public void setFlatNumber(String flatNumber) {
        this.flatNumber = flatNumber;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    @Override
    public String toString() {
        return String.format(
                "Address: \n%s %s %s \n%s %s \n%s",
                street,
                buildingNumber,
                flatNumber,
                postalCode,
                city,
                country
        );
    }
}
