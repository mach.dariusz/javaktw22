package com.sda.javaktw22.wprowadzenie.day2.lambdas;

@FunctionalInterface
public interface MyCustomFunctionalInterface {

    void foo();

}
