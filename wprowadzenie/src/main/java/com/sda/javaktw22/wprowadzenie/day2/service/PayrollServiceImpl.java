package com.sda.javaktw22.wprowadzenie.day2.service;

import com.sda.javaktw22.wprowadzenie.day2.model.Department;
import com.sda.javaktw22.wprowadzenie.day2.model.Employee;

public class PayrollServiceImpl implements PayrollService {

    private final static int TAX = 18;

    @Override
    public double calcSalary(Employee employee) {
        double salary = employee.getSalary() - employee.getSalary() * (TAX / 100.00);

        return salary;
    }

    @Override
    public double calcSalaryPerDept(Department dept) {
        return 0;
    }
}
