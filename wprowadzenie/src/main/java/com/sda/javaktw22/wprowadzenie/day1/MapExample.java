package com.sda.javaktw22.wprowadzenie.day1;

import java.util.HashMap;
import java.util.Map;

public class MapExample {

    static String[] namesArray = {
            "Ala", "Ola", "Ania", "Magda", "Ania", "Magda", "Ania", "Magda",
            "Ania", "Magda", "Ala", "Ola", "Ania", "Magda", "Ania", "Magda",
            "Ania", "Magda", "Ania", "Magda", "Ala", "Ola", "Ania", "Magda",
            "Ania", "Magda", "Ania", "Magda", "Ania", "Magda", "Ala", "Ola",
            "Ania", "Magda", "Ania", "Magda", "Ania", "Magda", "Ania", "Magda",
            "Ala", "Ola", "Ania", "Magda", "Ania", "Magda", "Ania", "Magda", "Magda",
            "Ania", "Magda", "Ala", "Ola", "Ania", "Magda", "Ania", "Magda",
            "Ania", "Magda", "Ania", "Magda", "Ala", "Ola", "Ania", "Magda",
            "Ania", "Magda", "Ania", "Magda", "Ania", "Magda", "Ala", "Ola",
            "Ania", "Magda", "Ania", "Magda", "Ania", "Magda", "Ania", "Magda", "Dariusz", "Darek"
    };

    public static void main(String[] args) {
        Map<String, Integer> map = new HashMap<>();

        for (String name : namesArray) {
            if (map.containsKey(name)) {
                int counter = map.get(name);
                counter++;
                map.put(name, counter);

                // alternative way - shorter
                // map.put(name, map.get(name) + 1);
            } else {
                map.put(name, 1);
            }
        }

        map.entrySet().stream().forEach(nameEntry ->
                System.out.println("Ilosc wystapien imienia: "
                        + nameEntry.getKey() + " wynosi: " + nameEntry.getValue())
        );

        System.out.println(map);

    }
}
