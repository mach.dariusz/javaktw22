package com.sda.javaktw22.wprowadzenie.day2.service;

import com.sda.javaktw22.wprowadzenie.day2.model.Department;
import com.sda.javaktw22.wprowadzenie.day2.model.Employee;

public interface PayrollService {

    double calcSalary(Employee employee);

    double calcSalaryPerDept(Department dept);
}
