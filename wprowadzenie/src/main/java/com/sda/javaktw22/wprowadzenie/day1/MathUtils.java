package com.sda.javaktw22.wprowadzenie.day1;

/**
 * @author mach.dariusz@gmail.com
 */
public class MathUtils {

    // silnia - rekurencyjna
    public static long calcFactorial(int liczba) throws WrongNumberException {
        if (liczba < 0) {
            throw new WrongNumberException("Nie mozna policzyc silni z liczby ujemnej!");
        }

        if (liczba == 0) {
            System.out.println("Zwracam 1");
            return 1;
        }

        System.out.println("Wykonuje obliczenie: " + liczba + " * calcFactorial(" + (liczba - 1) + ")");
        return liczba * calcFactorial(liczba - 1);
    }

    // silnia - petla for

    /**
     *
     * @param liczba
     * @return result of factorial calculation
     * @throws WrongNumberRuntimeException if liczba < 0
     */
    public static long calcFactorialWithForLoop(int liczba) {
        if (liczba < 0) {
            throw new WrongNumberRuntimeException();
        }

        long result = 1;

        for (int i = 1; i <= liczba; i++) {
            result *= i;
        }

        return result;
    }
}

class WrongNumberRuntimeException extends RuntimeException {

}


class WrongNumberException extends Exception {

    public WrongNumberException(String msg) {
        super(msg);
    }

    public WrongNumberException() {
        super();
    }
}