package com.sda.javaktw22.wprowadzenie.day1;

import java.util.HashSet;
import java.util.Set;

public class EmployeeTest {

    public static void main(String[] args) {
        Address address = new Address();
        address.setStreet("Żelazna");
        address.setBuildingNumber("38");
        address.setFlatNumber("4/A");
        address.setCity("Katowice");
        address.setPostalCode("40-450");
        address.setCountry("Polska");

        // boolean result = new Employee("Dariusz", "Mach")
        //         .equals(new Employee("Dariusz", "Mach"));
        // System.out.println(result);

        Set<Employee> employeeSet = new HashSet<>();
        employeeSet.add(new Employee("Dariusz", "Mach", address));
        employeeSet.add(new Employee("Dariusz", "Mach"));
        employeeSet.add(new Employee("Dariusz", "Mach"));
        employeeSet.add(new Employee("Dariusz", "Mach"));
        employeeSet.add(new Employee("Dariusz", "Mach", address));
        employeeSet.add(new Employee("Dariusz", "Mach"));
        employeeSet.add(new Employee("Dariusz", "Mach"));
        employeeSet.add(new Employee("Dariusz", "Mach"));
        employeeSet.add(new Employee("Dariusz", "Mach", address));

        System.out.println(employeeSet.size());
    }
}
