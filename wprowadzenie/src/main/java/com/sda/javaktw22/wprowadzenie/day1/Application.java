package com.sda.javaktw22.wprowadzenie.day1;

public class Application {

    public static void main(String[] args) {
        String kto = "World";
        System.out.printf("Hello %s!\n", kto);

        try {
            MathUtils.calcFactorial(-1);
        } catch (WrongNumberException e) {
            e.printStackTrace();
        } catch (Exception e) {
            // handle Exception here

        } finally {
            System.out.println("finally runs");
        }
        MathUtils.calcFactorialWithForLoop(-1);

        System.out.println("koniec");
    }
}
