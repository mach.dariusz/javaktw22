package com.sda.javaktw22.wprowadzenie.day2.lambdas;

import com.sda.javaktw22.wprowadzenie.day2.model.Employee;
import com.sda.javaktw22.wprowadzenie.day2.service.EmployeeService;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Example1 {

    public static void main(String[] args) {
        int[] arrInt = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        Integer[] arr = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        System.out.println(Arrays.toString(arr));

        List<Integer> integerList = Arrays.asList(arr);
        System.out.println(integerList);

        List<Integer> integerList2 = Arrays.stream(arrInt).boxed().collect(Collectors.toList());
        System.out.println(integerList2);

        // alternative to above
        List<Integer> newListOfInteger = new ArrayList<>();
        for (int i = 0; i < arrInt.length; i++) {
            newListOfInteger.add(arrInt[i]);
        }

        integerList.stream().forEach(item -> System.out.println(item));

        integerList.stream().forEach(integer -> {
            System.out.println(integer);
            // do something here
        });

        // example how throws NullPointerException
        // Employee e1 = EmployeeService.getEmployee(1024);
        // System.out.println(e1.getId());


        int a = integerList.stream().filter(item -> item > 9).findAny().orElse(-1);
        System.out.println(a);

        System.out.println(integerList.stream().count());
        System.out.println(integerList.stream().filter(item -> item > 4).count());

        List<Employee> list = integerList.stream().filter(item -> item > 2).map(item -> {
            Employee employee = new Employee();
            employee.setFirstName("" + item);
            employee.setSalary(item * 1000.00);

            return employee;
        }).collect(Collectors.toList());

        list.forEach(item -> System.out.println(item));
        list.forEach(System.out::println);
    }
}
