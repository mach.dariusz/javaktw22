package com.sda.javaktw22.wprowadzenie.day2.model;

public enum Department {
    HR,
    IT,
    PAYROLL;
}
