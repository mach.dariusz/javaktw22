package com.sda.javaktw22.wprowadzenie.day2.service;

import com.sda.javaktw22.wprowadzenie.day2.model.Employee;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

public class EmployeeService {

    private static final Map<Long, Employee> EMPLOYEE_MAP = new HashMap<>();
    private static final AtomicLong COUNTER = new AtomicLong(0);

    public static long getUniqueId() {
        return COUNTER.incrementAndGet();
    }

    public static List<Employee> getAllEmployees() {
        return new ArrayList<>(EMPLOYEE_MAP.values());
    }

    public static Employee getEmployee(long id) {
        return EMPLOYEE_MAP.get(id);
    }

    public static void addEmployee(Employee employee) {
        EMPLOYEE_MAP.put(employee.getId(), employee);
    }

    public static boolean removeEmployee(long id) {
        if (EMPLOYEE_MAP.remove(id) != null) {
            return true;
        } else {
            return false;
        }
    }

    public static void cleanEmployeesMap() {
        EMPLOYEE_MAP.clear();
        COUNTER.set(0);
    }
}
