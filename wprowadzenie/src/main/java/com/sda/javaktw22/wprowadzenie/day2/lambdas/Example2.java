package com.sda.javaktw22.wprowadzenie.day2.lambdas;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Example2 {

    private static Integer[] arrayOfIntegers = {10, 20, 30, 40, 50, 765, 890, 123, 1024, 567890};

    public static void main(String[] args) {
        // wydrukuje 10 cyfr
        task1();

        // przekszalic 10 cyfr na 10 stringow i zwrocic liste
        task2();
    }

    private static void task1() {
        Arrays.asList(arrayOfIntegers).forEach(System.out::println);

        // same as above
        // Arrays.stream(arrayOfIntegers).forEach(item -> System.out.println(item));
    }

    private static void task2() {

        List<String> stringList = Arrays.stream(arrayOfIntegers).map(iteger -> "" + iteger).collect(Collectors.toList());
        stringList.forEach(System.out::println);
    }
}
